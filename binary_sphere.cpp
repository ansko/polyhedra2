#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/algorithms/cross_segment_plane.hpp"
#include "src/algorithms/det2.hpp"
#include "src/algorithms/det3.hpp"
#include "src/algorithms/point_inside_polygon.hpp"
#include "src/algorithms/point_inside_polyhedron.hpp"
#include "src/algorithms/polygon_crosses_plane.hpp"
#include "src/algorithms/polygon_crosses_polygon.hpp"
#include "src/algorithms/polyhedron_inside_polyhedron.hpp"
#include "src/algorithms/segment_crosses_polygon.hpp"

#include "src/constructors/binary_composite.hpp"

#include "src/geometry/multipolyhedron.hpp"
#include "src/geometry/orthobrick.hpp"
#include "src/geometry/plane.hpp"
#include "src/geometry/point.hpp"
#include "src/geometry/polygon.hpp"
#include "src/geometry/polygonal_cylinder.hpp"
#include "src/geometry/polygonal_sphere.hpp"
#include "src/geometry/polyhedron.hpp"
#include "src/geometry/segment.hpp"


int main()
{
    srand (time(NULL));

    PolygonalSphere ps;
    Orthobrick ob(Point(-10,-10,-10), Point(10,10,10));
    PolygonalCylinder pc1(2,0.5,4);
    PolygonalCylinder pc2(2,0.5,16);
    PolygonalCylinder pc3(5,0.5,3);
    std::vector<std::pair<std::shared_ptr<Polyhedron>, int> > hf;
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc1),10));
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc2),10));
    hf.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>(std::make_shared<Polyhedron>(pc3),3));
    MultiPolyhedron bin = binary_composite(ob, hf);

    std::ofstream ofs("test.geo");
    /*
    std::cout << "algebraic3d\n\n";
    std::cout << bin << std::endl;
    std::cout << "\ntlo filler;\ntlo matrix -transparent;\n";
    */
    ofs << "algebraic3d\n\n";
    ofs << bin << std::endl;
    ofs << "\ntlo filler;\ntlo matrix -transparent;\n";

    return 0;
}
