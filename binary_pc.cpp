#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "src/algorithms/cross_segment_plane.hpp"
#include "src/algorithms/det2.hpp"
#include "src/algorithms/det3.hpp"
#include "src/algorithms/point_inside_polygon.hpp"
#include "src/algorithms/point_inside_polyhedron.hpp"
#include "src/algorithms/polygon_crosses_plane.hpp"
#include "src/algorithms/polygon_crosses_polygon.hpp"
#include "src/algorithms/polyhedron_inside_polyhedron.hpp"
#include "src/algorithms/segment_crosses_polygon.hpp"

#include "src/constructors/binary_composite.hpp"

#include "src/geometry/multipolyhedron.hpp"
#include "src/geometry/orthobrick.hpp"
#include "src/geometry/plane.hpp"
#include "src/geometry/point.hpp"
#include "src/geometry/polygon.hpp"
#include "src/geometry/polygonal_cylinder.hpp"
#include "src/geometry/polyhedron.hpp"
#include "src/geometry/segment.hpp"



// Short name for the type to represent "Binary Particle"
typedef std::pair<std::shared_ptr<Polyhedron>,int> BP;


int main(int argc, char **argv)
{
    srand (time(NULL));

    size_t N = atoi(argv[1]);
    float L = 40;  // in my calculations cubic box is used, it's easy to use such a variable
    float r, R, h, H;
    size_t P = 16;  // Number of vertices in top and bottom polygons
    r=12.5; h=1; R=13.0; H=2;

    Orthobrick ob(Point(-L/2,-L/2,-L/2), Point(L/2,L/2,L/2));
    PolygonalCylinder pc1(r, h, P);

    {
        float central_angle = 2*M_PI / P;
        float fi_appro = N * 0.5*P*r*r*sin(central_angle) * h / (L*L*L);
        std::cout << fi_appro << std::endl;
    }

    std::vector<BP> hysto;
    hysto.push_back(std::make_pair<std::shared_ptr<Polyhedron>, int>( 
        std::make_shared<Polyhedron>(pc1),N));
    MultiPolyhedron bin = binary_composite(ob, hysto);

    std::ofstream ofs("test.geo");
    ofs << "algebraic3d\n\n";
    ofs << bin << std::endl;
    ofs << "\ntlo filler;"
        << "\ntlo matrix -transparent;\n";

    std::cout << "----------\n";
    std::cout << bin.phases_content()["filler"].size() << " of " << N << std::endl;

    return 0;
}
