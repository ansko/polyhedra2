#ifndef SRC_ALGORITHMS_SEGMENT_CROSSES_POLYGON_HPP
#define SRC_ALGORITHMS_SEGMENT_CROSSES_POLYGON_HPP


#include "../geometry/segment.hpp"
#include "../geometry/polygon.hpp"
#include "cross_segment_plane.hpp"
#include "point_inside_polygon.hpp"


bool segment_crosses_polygon(const Segment &seg, const Polygon &poly)
{
    // Check wheter segment crosses plane containing polygon.
    // If does, check whether crossing point is inside the polygon
    Plane pl({poly.center(), poly.vertices()[0], poly.vertices()[1]});
    std::shared_ptr<Point> cross_plane_ptr = cross_segment_plane(seg, pl);
    if (cross_plane_ptr == nullptr)
        return false;

    return point_inside_polygon(*cross_plane_ptr, poly);
}


#endif  // #ifndef SRC_ALGORITHMS_SEGMENT_CROSSES_POLYGON_HPP
