#ifndef SRC_ALGORITHMS_POLYHEDRON_INSIDE_POLYHEDRON_HPP
#define SRC_ALGORITHMS_POLYHEDRON_INSIDE_POLYHEDRON_HPP


#include "../geometry/point.hpp"
#include "../geometry/polyhedron.hpp"
#include "point_inside_polyhedron.hpp"


bool polyhedron_inside_polyhedron(Polyhedron &ph_in, Polyhedron &ph_out)
{
    // Check whether every vertex of ph_in is inside ph_out
    for (const Polygon &p : ph_in.facets())
        for (const Point &v : p.vertices())
            if (!point_inside_polyhedron(v, ph_out))
                return false;

    return true;
}


#endif  // #ifndef SRC_ALGORITHMS_POLYHEDRON_INSIDE_POLYHEDRON_HPP
