#ifndef SRC_ALGORITHMS_POINT_INSIDE_POLYGON_HPP
#define SRC_ALGORITHMS_POINT_INSIDE_POLYGON_HPP


#include "../geometry/point.hpp"
#include "../geometry/polygon.hpp"


bool point_inside_polygon(const Point &p, const Polygon &poly, float epsilon=1e-6)
{
    float s_poly = 0;
    float s_poly_pt = 0;
    Point p_c = poly.center();

    // Roughly: point inside outer circumference
    {
        // Calculate distance from point to polygon center
        float dx_circ = p_c.x() - p.x();
        float dy_circ = p_c.y() - p.y();
        float dz_circ = p_c.z() - p.z();
        float dr_circ2 = dx_circ*dx_circ + dy_circ*dy_circ + dz_circ*dz_circ;
        // Calculate outer circumference radius as
        // distance from polygon center to its 0th vertex
        float r_x = p_c.x() - poly.vertices()[0].x();
        float r_y = p_c.y() - poly.vertices()[0].y();
        float r_z = p_c.z() - poly.vertices()[0].z();
        float r2 = r_x*r_x + r_y*r_y + r_z*r_z;
        return dr_circ2 < r2;
    }

    // Calculate S_polygon
    // Calculate Sum of triangles: (pt, v_i, v_i+1)
    // If these areas equal, point is inside polygon
    for (size_t idx = 0; idx < poly.vertices().size(); ++idx)
      {
        size_t idx_i, idx_j;
        if (idx < poly.vertices().size() - 1)
          {
            idx_i = idx;
            idx_j = idx + 1;
          }
        else
          {
            idx_i = idx;
            idx_j = 0;
          }
        Point p_i = poly.vertices()[idx_i];
        Point p_j = poly.vertices()[idx_j];

        {
            float dxij = p_j.x() - p_i.x();
            float dyij = p_j.y() - p_i.y();
            float dzij = p_j.z() - p_i.z();
            float drij = sqrt(dxij*dxij + dyij*dyij + dzij*dzij);
            float dxic = p_c.x() - p_i.x();
            float dyic = p_c.y() - p_i.y();
            float dzic = p_c.z() - p_i.z();
            float dric = sqrt(dxic*dxic + dyic*dyic + dzic*dzic);
            float dxjc = p_c.x() - p_j.x();
            float dyjc = p_c.y() - p_j.y();
            float dzjc = p_c.z() - p_j.z();
            float drjc = sqrt(dxjc*dxjc + dyjc*dyjc + dzjc*dzjc);
            float p = (drij + dric + drjc)/2;
            float s = sqrt(p*(p-drij)*(p-dric)*(p-drjc));
            s_poly += s;
        }
        {
            float dxij = p_j.x() - p_i.x();
            float dyij = p_j.y() - p_i.y();
            float dzij = p_j.z() - p_i.z();
            float drij = sqrt(dxij*dxij + dyij*dyij + dzij*dzij);
            float dxip = p.x() - p_i.x();
            float dyip = p.y() - p_i.y();
            float dzip = p.z() - p_i.z();
            float drip = sqrt(dxip*dxip + dyip*dyip + dzip*dzip);
            float dxjp = p.x() - p_j.x();
            float dyjp = p.y() - p_j.y();
            float dzjp = p.z() - p_j.z();
            float drjp = sqrt(dxjp*dxjp + dyjp*dyjp + dzjp*dzjp);
            float p = (drij + drip + drjp)/2;
            float s = sqrt(p*(p-drij)*(p-drip)*(p-drjp));
            s_poly_pt += s;
        }
      }

    s_poly = std::fabs(s_poly);
    s_poly_pt = std::fabs(s_poly_pt);

    return (s_poly_pt - s_poly) / (s_poly_pt + s_poly) < epsilon;
}


#endif  // #ifndef SRC_ALGORITHMS_POINT_INSIDE_POLYGON_HPP
