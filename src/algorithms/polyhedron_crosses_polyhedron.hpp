#ifndef SRC_ALGORITHMS_POLYHEDRON_CROSSES_POLYHEDRON_HPP
#define SRC_ALGORITHMS_POLYHEDRON_CROSSES_POLYHEDRON_HPP


#include "../geometry/polyhedron.hpp"
#include "polygon_crosses_polygon.hpp"


bool polyhedron_crosses_polyhedron(const Polyhedron &ph1, const Polyhedron &ph2)
{
    // Check wheteher any facet of one polygon crosses any facet of another polygon
    for (const Polygon &f1 : ph1.facets())
        for (const Polygon &f2 : ph2.facets())
            if (polygon_crosses_polygon(f1,f2))
                return true;

    return false;
}


#endif  // #ifndef SRC_ALGORITHMS_POLYHEDRON_CROSSES_POLYHEDRON_HPP
