#ifndef SRC_ALGORITHMS_POLYGON_CROSSES_PLANE_HPP
#define SRC_ALGORITHMS_POLYGON_CROSSES_PLANE_HPP


#include "../geometry/plane.hpp"
#include "../geometry/polygon.hpp"
#include "cross_segment_plane.hpp"
#include "det3.hpp"



bool polygon_crosses_plane(const Polygon &poly, const Plane& pl)
{
    for (size_t idx = 0; idx < poly.vertices().size(); ++idx)
      {
        size_t idx_a, idx_b;
        if (idx < poly.vertices().size() - 1)
          {
            idx_a = idx;
            idx_b = idx + 1;
          }
        else
          {
            idx_a = idx;
            idx_b = 0;
          }

        Point A = poly.vertices()[idx_a];
        Point B = poly.vertices()[idx_b];
        Segment seg(A,B);
        if (cross_segment_plane(seg, pl) != nullptr)
            return true;
      }

    return false;
}


#endif  // ifndef SRC_ALGORITHMS_POLYGON_CROSSES_PLANE_HPP
