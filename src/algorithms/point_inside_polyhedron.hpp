#ifndef SRC_ALGORITHMS_POINT_INSIDE_POLYHEDRON_HPP
#define SRC_ALGORITHMS_POINT_INSIDE_POLYHEDRON_HPP


#include "../geometry/point.hpp"
#include "../geometry/polyhedron.hpp"
#include "segment_crosses_polygon.hpp"


bool point_inside_polyhedron(const Point &pt, const Polyhedron &ph)
{
    // Check wheter segment from point to the polyhedron center crosses some polyhedron facet
    Segment seg(pt, ph.center());
    for (const Polygon &poly : ph.facets())
      {
        if (segment_crosses_polygon(seg, poly))
            return false;
      }

    return true;
}

#endif  // #ifndef SRC_ALGORITHMS_POINT_INSIDE_POLYHEDRON_HPP
