#ifndef SRC_GEOMETRY_PLANE_HPP
#define SRC_GEOMETRY_PLANE_HPP


#include <memory>
#include <random>

#include "../algorithms/det2.hpp"
#include "../algorithms/det3.hpp"
#include "point.hpp"
#include "segment.hpp"


class Plane
{
public:
    Plane(const Point &pt_0, const Point &pt_1, const Point &pt_2)
    : _pt_0(pt_0), _pt_1(pt_1), _pt_2(pt_2)  {
        float dx01 = pt_1.x() - pt_0.x();
        float dy01 = pt_1.y() - pt_0.y();
        float dz01 = pt_1.z() - pt_0.z();
        float dr01 = sqrt(dx01*dx01 + dy01*dy01 + dz01*dz01);
        float dx02 = pt_2.x() - pt_0.x();
        float dy02 = pt_2.y() - pt_0.y();
        float dz02 = pt_2.z() - pt_0.z();
        float dr02 = sqrt(dx02*dx02 + dy02*dy02 + dz02*dz02);
        float product = std::fabs(dx01*dx02 + dy01*dy02 + dz01*dz02);
        if (std::fabs(product/dr01/dr02 - 1) < 1e-6)
            throw std::string("Exception! "
                              "All 3 points are on a single line in "
                              "Plane::Plane(const Point&, const Point&, const Point&).");
      }

    const Point pt_0() const { return _pt_0; }
    const Point pt_1() const { return _pt_1; }
    const Point pt_2() const { return _pt_2; }

    const Segment normal() const
      {
        float dx01 = _pt_1.x() - _pt_0.x();
        float dy01 = _pt_1.y() - _pt_0.y();
        float dz01 = _pt_1.z() - _pt_0.z();
        float dx02 = _pt_2.x() - _pt_0.x();
        float dy02 = _pt_2.y() - _pt_0.y();
        float dz02 = _pt_2.z() - _pt_0.z();
        float dx = dy01*dz02 - dz01*dy02;
        float dy = dz01*dx02 - dx01*dz02;
        float dz = dx01*dy02 - dx02*dy01;
        return Segment(_pt_0, Point(_pt_0.x()+dx, _pt_0.y()+dy, _pt_0.z()+dz));
      }

    void translate(const float dx, const float dy, const float dz)
      {
        _pt_0.translate(dx, dy, dz);
        _pt_1.translate(dx, dy, dz);
        _pt_2.translate(dx, dy, dz);
      }

    void rotate(const Point &rot_c, const float ax, const float ay, const float az)
      {
        _pt_0.rotate(rot_c, ax, ay, az);
        _pt_1.rotate(rot_c, ax, ay, az);
        _pt_2.rotate(rot_c, ax, ay, az);
      }
private:
    Point _pt_0;
    Point _pt_1;
    Point _pt_2;
};


// CSG-ready
std::ostream& operator<<(std::ostream& os, const Plane& pl)
{
    os << "plane(" << pl.pt_0() << "; "
                   << pl.normal().lx() << ", "
                   << pl.normal().ly() << ", "
                   << pl.normal().lz() << ")";
    return os;
}


#endif  // #ifndef SRC_GEOMETRY_PLANE_HPP
