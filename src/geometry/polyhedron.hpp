#ifndef SRC_GEOMETRY_PLYHEDRON_HPP
#define SRC_GEOMETRY_PLYHEDRON_HPP


#include <memory>
#include <sstream>
#include <vector>

#include "polygon.hpp"
#include "../algorithms/det3.hpp"


class Polyhedron
{
public:
    Polyhedron() {}; // ?

    Polyhedron(const Polyhedron &ph)
    : _facets(ph.facets())
    {}


    Polyhedron(const std::vector<Polygon> facets)
    : _facets(facets) {}

    const std::vector<Polygon> facets() const
      {
        if (_facets.size() == 0)
            throw std::string("Exception in Polyhedron::center(): "
                              "polygon is not properly constructed");
        return _facets;
      }

    const Point center() const
      {
        if (_facets.size() == 0)
            throw std::string("Exception in Polyhedron::center(): "
                              "polygon is not properly constructed");

        float x = 0;
        float y = 0;
        float z = 0;
        for (const Polygon &p : _facets)
          {
            const Point c = p.center();
            x += c.x();
            y += c.y();
            z += c.z();
          }
        x /= _facets.size();
        y /= _facets.size();
        z /= _facets.size();
        return Point(x,y,z);            
      }

    void translate(const float dx, const float dy, const float dz)
      {
        if (_facets.size() == 0)
            throw std::string("Exception in Polyhedron::center(): "
                              "polygon is not properly constructed");

        for (Polygon &face : _facets)
            face.translate(dx, dy, dz);
      }
    void rotate(const Point &rot_c, const float ax, const float ay, const float az)
      {
        if (_facets.size() == 0)
            throw std::string("Exception in Polyhedron::center(): "
                              "polygon is not properly constructed");

        for (Polygon &face : _facets)
            face.rotate(rot_c, ax, ay, az);
      }

protected:
    std::vector<Polygon> _facets;
};


/*
// CSG-consistent via triangles (pts; ...; triangles; ...)
// Does not work for some unclear reason (though i think, it is correct method)
std::ostream& operator<<(std::ostream& os, const Polyhedron& ph)
{
    const float epsilon = 1e-6;
    const Point c = ph.center();

    os << "polyhedron(";

    std::vector<Point> all_points;
    for (const Polygon &poly : ph.facets())
        for (const Point &pt : poly.vertices())
          {
            bool pt_present = false;
            for (const Point &old_pt : all_points)
              {
                const float dx = pt.x() - old_pt.x();
                const float dy = pt.y() - old_pt.y();
                const float dz = pt.z() - old_pt.z();
                const float dr2 = dx*dx + dy*dy + dz*dz;
                if (dr2 < epsilon)
                  {
                    pt_present = true;
                    break;
                  }
              }
            if (!pt_present)
                all_points.push_back(pt);
          }

    for (const Polygon &p : ph.facets())
        all_points.push_back(p.center());

    for (size_t idx = 0; idx < all_points.size(); ++idx)
        os << all_points[idx] << ";";

    for (const Polygon &p : ph.facets())
      {
        const Point pc = p.center();

        std::shared_ptr<size_t> pt_idx_c_ptr;
        for (size_t idx_all_pts = 0; idx_all_pts < all_points.size(); ++idx_all_pts)
          {
            const float dxc = all_points[idx_all_pts].x() - pc.x();
            const float dyc = all_points[idx_all_pts].y() - pc.y();
            const float dzc = all_points[idx_all_pts].z() - pc.z();
            const float dr2c = dxc*dxc + dyc*dyc + dzc*dzc;
            if (dr2c < epsilon)
              pt_idx_c_ptr = std::make_shared<size_t>(idx_all_pts + 1);
          }
        if (pt_idx_c_ptr == nullptr)
            throw std::string("Exteption in std::ostream& operator<<(std::ostream&, const Polyhedron&).");

        for (size_t idx_vert = 0; idx_vert < p.vertices().size(); ++idx_vert)
          {
            size_t idx_i, idx_j;
            if (idx_vert < p.vertices().size()-1)
              {
                idx_i = idx_vert;
                idx_j = idx_vert+1;
              }
            else
              {
                idx_i = idx_vert;
                idx_j = 0;
              }

            // From c to pc - v_i - v_j
            float c_pc_x = pc.x() - c.x();
            float c_pc_y = pc.y() - c.y();
            float c_pc_z = pc.z() - c.z();
            float c_v_i_x = p.vertices()[idx_i].x() - c.x();
            float c_v_i_y = p.vertices()[idx_i].y() - c.y();
            float c_v_i_z = p.vertices()[idx_i].z() - c.z();
            float c_v_j_x = p.vertices()[idx_j].x() - c.x();
            float c_v_j_y = p.vertices()[idx_j].y() - c.y();
            float c_v_j_z = p.vertices()[idx_j].z() - c.z();

            float d = det3(c_pc_x,  c_pc_y,  c_pc_z,
                           c_v_i_x, c_v_i_y, c_v_i_z,
                           c_v_j_x, c_v_j_y, c_v_j_z);

            std::shared_ptr<size_t> pt_idx_i_ptr;
            std::shared_ptr<size_t> pt_idx_j_ptr;
            for (size_t idx_all_pts = 0; idx_all_pts < all_points.size(); ++idx_all_pts)
              {
                const float dxi = all_points[idx_all_pts].x() - p.vertices()[idx_i].x();
                const float dyi = all_points[idx_all_pts].y() - p.vertices()[idx_i].y();
                const float dzi = all_points[idx_all_pts].z() - p.vertices()[idx_i].z();
                const float dr2i = dxi*dxi + dyi*dyi + dzi*dzi;
                const float dxj = all_points[idx_all_pts].x() - p.vertices()[idx_j].x();
                const float dyj = all_points[idx_all_pts].y() - p.vertices()[idx_j].y();
                const float dzj = all_points[idx_all_pts].z() - p.vertices()[idx_j].z();
                const float dr2j = dxj*dxj + dyj*dyj + dzj*dzj;
                if (dr2i < epsilon)
                    pt_idx_i_ptr = std::make_shared<size_t>(idx_all_pts + 1);
                if (dr2j < epsilon)
                    pt_idx_j_ptr = std::make_shared<size_t>(idx_all_pts + 1);
              }
            if (pt_idx_i_ptr == nullptr)
                throw std::string("Exteption in std::ostream& operator<<(std::ostream&, const Polyhedron&).");
            if (pt_idx_j_ptr == nullptr)
                throw std::string("Exteption in std::ostream& operator<<(std::ostream&, const Polyhedron&).");


            if (d > 0)
                os << "; " << *pt_idx_i_ptr << "," << *pt_idx_j_ptr << "," << *pt_idx_c_ptr;
            else
                os << "; " << *pt_idx_j_ptr << "," << *pt_idx_i_ptr << "," << *pt_idx_c_ptr;
          }
      }
    os << ")";

    return os;
}
*/


// CSG-consistent via planes (each polygon is printed as plane where it lies)
std::ostream& operator<<(std::ostream& os, const Polyhedron& ph)
{
    const Point c = ph.center();

    for(size_t idx = 0; idx < ph.facets().size(); ++idx)
      {
        const Point pc = ph.facets()[idx].center();
        const float dx = pc.x() - c.x();
        const float dy = pc.y() - c.y();
        const float dz = pc.z() - c.z();
        if (idx != 0)
            os << " and ";
        os << "plane(" << pc.x() << "," << pc.y() << "," << pc.z() << ";"
           << dx << "," << dy << "," << dz << ")";
      }

    return os;
}


#endif  // #ifndef SRC_GEOMETRY_PLYHEDRON_HPP
