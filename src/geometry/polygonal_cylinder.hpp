#ifndef SRC_GEOMETRY_POLYGONAL_CYLINDER_HPP
#define SRC_GEOMETRY_POLYGONAL_CYLINDER_HPP

#include <cmath>
#include <vector>

//#include "../geometry/point.hpp"
//#include "../geometry/polygon.hpp"
#include "polyhedron.hpp"


// Polygonal cylinder is a cylinder which base is a regular polygon
// and which height is perpedicular to its base. The cylinder always has
// a center at the origin. It may be further transformed (rotate + tranlate).
//     float H - the height of the cylinder
//     float R - outer circumference radius of the base polygon
//     size_t N - number of vertices of its basess
class PolygonalCylinder : public Polyhedron
{
public:
    // Oriented across z initially
    PolygonalCylinder(float R, float H, size_t N)
      {
        std::vector<Point> top_vs, bottom_vs;

        for (size_t idx = 0; idx < N; ++idx)
          {
            float x = R * cos(idx * 2*M_PI/N);
            float y = R * sin(idx * 2*M_PI/N);
            top_vs.push_back(Point(x,y,H/2));
            bottom_vs.push_back(Point(x,y,-H/2));
          }

        _facets.push_back(Polygon(top_vs));
        _facets.push_back(Polygon(bottom_vs));
        for (size_t idx = 0; idx < N; ++idx)
          {
            size_t idx_i, idx_j;
            if (idx < N-1)
              {
                idx_i = idx;
                idx_j = idx+1;
              }
            else // idx == N-1
              {
                idx_i = idx;
                idx_j = 0;
              }
            _facets.push_back(Polygon({bottom_vs[idx_i],
                                       top_vs[idx_i],
                                       top_vs[idx_j],
                                       bottom_vs[idx_j]}));
          }
      }
};


#endif  // #ifndef SRC_GEOMETRY_POLYGONAL_CYLINDER_HPP
