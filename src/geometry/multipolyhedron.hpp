#ifndef SRC_GEOMETRY_MULTIPOLYHEDRON_HPP
#define SRC_GEOMETRY_MULTIPOLYHEDRON_HPP


#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <string>

#include "polyhedron.hpp"


// Wrapper of the map <string, objects>
// Binary and ternary systems classes to be derived from this.
class MultiPolyhedron
{
public:
    void set_phases_hierarchy(std::vector<std::string> phases_hierarchy)
      {
        _phases_hierarchy = phases_hierarchy;
      }

    std::vector<std::string> phases_hierarchy() const
      {
        return _phases_hierarchy;
      }

    void insert_particle(std::string phase_name, Polyhedron &particle)
      {
        bool phase_existed = false;
        for (auto const &kv : _phases_content)
            if (kv.first == phase_name)
              {
                _phases_content[phase_name].push_back(particle);
                phase_existed = true;
              }
        if (phase_existed)
            return;
        _phases_content[phase_name] = std::vector<Polyhedron>();
        _phases_content[phase_name].push_back(particle);
      }

    std::map<std::string, std::vector<Polyhedron> >& phases_content()
      { 
        return _phases_content;
      }

private:
    std::vector<std::string> _phases_hierarchy;
    std::map<std::string, std::vector<Polyhedron> > _phases_content;
};


std::ostream& operator<<(std::ostream& os, MultiPolyhedron& mp)
{
    if (mp.phases_content().size() == 0)
        return os;

    std::vector<std::string> phases_hierarchy = mp.phases_hierarchy();

    for (auto kv : mp.phases_content())
      {
        os << "solid " << kv.first << "_tmp = (";
        for (size_t idx = 0; idx < kv.second.size(); ++idx)
          {
            os << kv.second[idx];
            if (idx != kv.second.size() - 1)
                os << ") or (";
          }
        os << ");\n";
      }

    std::vector<std::string> phases_names;
    for (auto kv : mp.phases_content())
        phases_names.push_back(kv.first);

    for (size_t idx_a = 0; idx_a < phases_names.size(); ++idx_a)
      {
        auto it_a = std::find(phases_hierarchy.begin(), phases_hierarchy.end(), phases_names[idx_a]);
        if (it_a == phases_hierarchy.end())
            throw std::string("Exception in std::ostream& operator<<(std::ostream&, MultiPolyhedron&): "
                              "incorrect phases hierachy");
        size_t hierarchy_a = std::distance(phases_hierarchy.begin(), it_a);

        os << "solid " << phases_names[idx_a] << " = " << phases_names[idx_a] << "_tmp";
        for (size_t idx_b = 0; idx_b < phases_names.size(); ++idx_b)
          {
            if (idx_b == idx_a)
                continue;

            auto it_b = std::find(phases_hierarchy.begin(), phases_hierarchy.end(), phases_names[idx_b]);
            if (it_b == phases_hierarchy.end())
                throw std::string("Exception in std::ostream& operator<<(std::ostream&, MultiPolyhedron&): "
                                  "incorrect phases hierachy");
            size_t hierarchy_b = std::distance(phases_hierarchy.begin(), it_b);

            if (hierarchy_b > hierarchy_a)
                os << " and not " << phases_names[idx_b] << "_tmp";
          }
        os << ";\n";
      }

    return os;
}


#endif  // #ifndef SRC_GEOMETRY_MULTIPOLYHEDRON_HPP
