#ifndef SRC_GEOMETRY_POLYGON_HPP
#define SRC_GEOMETRY_POLYGON_HPP


#include <cmath>
#include <vector>

#include "segment.hpp"
#include "plane.hpp"
#include "point.hpp"

#include "../algorithms/det2.hpp"
#include "../algorithms/det3.hpp"


class Polygon {
public:
    Polygon(const std::vector<Point> &vertices)
    : _vertices(vertices)
      {
        if (vertices.size() < 3)
            throw std::string("Exception! "
                              "Polygon contains to few points in "
                              "Polygon::Polygon(const std::vector<Point>)");

        check_planarity();
        check_vertices_enumeration(); // maybe add re-enumeration ?
      };

    void check_planarity() const
      {
        // ... precision?
        float epsilon = 1e-3;

        if (_vertices.size() == 3)
            return;

        Point pt0 = _vertices[0];
        Point pt1 = _vertices[1];
        Point pt2 = _vertices[2];

        float side = sqrt((pt0.x() - pt1.x())*(pt0.x() - pt1.x()) + (pt0.y() - pt1.y())*(pt0.y() - pt1.y()) + (pt0.z() - pt1.z())*(pt0.z() - pt1.z()));

        for (size_t idx = 3; idx < _vertices.size(); ++idx)
          {
            Point pt4 = _vertices[idx];
            float d = det3(pt4.x()-pt0.x(), pt4.y()-pt0.y(), pt4.z()-pt0.z(),
                           pt4.x()-pt1.x(), pt4.y()-pt1.y(), pt4.z()-pt1.z(),
                           pt4.x()-pt2.x(), pt4.y()-pt2.y(), pt4.z()-pt2.z());
            if (std::fabs(d) > epsilon * side)
                throw std::string("Exception! Polygon planarity check failed. Error poosible, erorr calculated = ") + std::to_string(d) + " " + std::to_string(epsilon * side);
          }
      }

    void check_vertices_enumeration() const
      {
        if (_vertices.size() == 3)
            return;

        // n0 = (next-curr) x (prev-curr) 
        // ni = (next_i-curr_i) x (prev_i-curr_i)
        // -->> (n0, ni) > 0

        Point prev = _vertices[0];
        Point curr = _vertices[1];
        Point next = _vertices[2];

        float n0x = (next.y()-curr.y())*(prev.z()-curr.z()) - (prev.y()-curr.y())*(next.z()-curr.z());
        float n0y = (prev.x()-curr.x())*(next.z()-curr.z()) - (next.x()-curr.x())*(prev.z()-curr.z());
        float n0z = (next.x()-curr.x())*(prev.y()-curr.y()) - (prev.x()-curr.x())*(next.y()-curr.y());

        for (size_t idx = 3; idx < _vertices.size() + 3; ++idx)
          {
            prev = curr;
            curr = next;
            if (idx < _vertices.size())
                next = _vertices[idx];
            else
                next = _vertices[idx-_vertices.size()];

            float nix = (next.y()-curr.y())*(prev.z()-curr.z()) - (prev.y()-curr.y())*(next.z()-curr.z());
            float niy = (prev.x()-curr.x())*(next.z()-curr.z()) - (next.x()-curr.x())*(prev.z()-curr.z());
            float niz = (next.x()-curr.x())*(prev.y()-curr.y()) - (prev.x()-curr.x())*(next.y()-curr.y());

            if (n0x*nix + n0y*niy + n0z*niz < 0)
                throw std::string("Exception! Vertices are not numbered consecutiely in  "
                                  "Polygon::Polygon(const std::vector<Point>)");
          }

      }

    void check_vertices_enumeration2() const
      {
        if (_vertices.size() == 3)
            return;

        Point prev = _vertices[0];
        Point curr = _vertices[1];
        Point next = _vertices[2];

        std::cout << prev<< "; "<< curr <<"; "<< next <<std::endl;

        float product = (next.x()-curr.x())*(curr.x()-prev.x())
                        +(next.y()-curr.y())*(curr.y()-prev.y())
                        +(next.z()-curr.z())*(curr.z()-prev.z());
        product /= Segment(next, curr).length();
        product /= Segment(prev, curr).length();

        std::cout << product << std::endl;

        for (size_t idx = 3; idx < _vertices.size() + 3; ++idx)
          {
            prev = curr;
            curr = next;
            if (idx < _vertices.size())
                next = _vertices[idx];
            else
                next = _vertices[idx-_vertices.size()];

            float product_cur = (next.x()-curr.x())*(curr.x()-prev.x())
                                +(next.y()-curr.y())*(curr.y()-prev.y())
                                +(next.z()-curr.z())*(curr.z()-prev.z());
            product_cur /= Segment(next, curr).length();
            product_cur /= Segment(prev, curr).length();

            std::cout << product_cur << " | " << next.x()-curr.x() << "; "
                                              << next.y()-curr.y() << "; "
                                              << next.z()-curr.z() << "; "
                                              << curr.x()-prev.x() << "; "
                                              << curr.y()-prev.y() << "; "
                                              << curr.z()-prev.z() << "\n";

            if (product_cur*product < 0)
                throw std::string("Exception! "
                                  "Non-cnsecutive vertices enumeration in "
                                  "Polygon::Polygon(const std::vector<Point>).");
          }
      }

    const Point center() const
      {
        float x_c = 0;
        float y_c = 0;
        float z_c = 0;
        for (const Point &pt : _vertices)
          {
            x_c += pt.x();
            y_c += pt.y();
            z_c += pt.z();
          }
        x_c /= _vertices.size();
        y_c /= _vertices.size();
        z_c /= _vertices.size();
        return Point(x_c, y_c, z_c);
      }

    const std::vector<Point> vertices() const { return _vertices; }

    void translate(const float dx, const float dy, const float dz)
      {
        for (Point &pt : _vertices)
            pt.translate(dx, dy, dz);
      }

    void rotate(const Point &rot_c, const float ax, const float ay, const float az)
      {
        for (Point &pt : _vertices)
            pt.rotate(rot_c, ax, ay, az);
      }

private:
    std::vector<Point> _vertices;
};


// Not CSG-ready since there is no such primitive as polygon in netgen
std::ostream& operator<<(std::ostream& os, const Polygon& poly)
{
    os << "polygon(";
    const std::vector<Point> vs = poly.vertices();
    for (size_t idx = 0; idx < vs.size(); ++idx)
      {
         os << vs[idx];
         if (idx != vs.size()-1)
             os << "; ";
      }
    os << ")";
    return os;
}


#endif  // #ifndef SRC_GEOMETRY_POLYGON_HPP
