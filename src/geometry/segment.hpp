#ifndef SRC_GEOMETRY_SEGMENT_HPP
#define SRC_GEOMETRY_SEGMENT_HPP

#include <cmath>
#include <memory>
#include <string>

#include "point.hpp"


class Segment
{
public:
    Segment(const Point &pt_beg, const Point &pt_end)
    : _pt_beg_ptr(std::make_shared<Point>(pt_beg)),
      _pt_end_ptr(std::make_shared<Point>(pt_end))
    {}

    const float lx() const
      {
        if (_pt_beg_ptr != nullptr && _pt_end_ptr != nullptr)
          {
            return std::fabs(_pt_beg_ptr->x() - _pt_end_ptr->x());
          }
        else
            throw std::string("Exception! "
                              "Access to null pointer in Segment::lx().");
      };

    const float ly() const
      {
        if (_pt_beg_ptr != nullptr && _pt_end_ptr != nullptr)
          {
            return std::fabs(_pt_beg_ptr->y() - _pt_end_ptr->y());
          }
        else
            throw std::string("Exception! "
                              "Access to null pointer in Segment::ly().");
      };

    const float lz() const
      {
        if (_pt_beg_ptr != nullptr && _pt_end_ptr != nullptr)
          {
            return std::fabs(_pt_beg_ptr->z() - _pt_end_ptr->z());
          }
        else
            throw std::string("Exception! "
                              "Access to null pointer in Segment::lz().");
      };

    const float length() const
      {
        try
          {
            return sqrt(this->lx()*this->lx() + this->ly()*this->ly() + this->lz()*this->lz());
          }
        catch (const std::string ex)
          {
            throw std::string("Exception! "
                              "Access to null pointer in Segment::length() after previous: ")
                  + ex;
          }
      };
    const Point pt_beg() const { return *_pt_beg_ptr; }
    const Point pt_end() const { return *_pt_end_ptr; }

    void translate(const float dx, const float dy, const float dz)
      {
        _pt_beg_ptr->translate(dx, dy, dz);
        _pt_end_ptr->translate(dx, dy, dz);
      }

    void rotate(const Point &rot_c, const float ax, const float ay, const float az)
      {
        _pt_beg_ptr->rotate(rot_c, ax, ay, az);
        _pt_end_ptr->rotate(rot_c, ax, ay, az);
      }
private:
    std::shared_ptr<Point> _pt_beg_ptr;
    std::shared_ptr<Point> _pt_end_ptr;
};


// Not CSG-ready since there is not such primitive in netgen
std::ostream& operator<<(std::ostream& os, const Segment& seg)
{
    os << "Segment{(" << seg.pt_beg() << ") -> (" << seg.pt_end() << ")}";
    return os;
}

#endif  // ifndef SRC_GEOMETRY_SEGMENT_HPP
