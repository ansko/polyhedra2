#ifndef SRC_CONSTRUCTORS_TERNARY_COMPOSITE_HPP
#define SRC_CONSTRUCTORS_TERNARY_COMPOSITE_HPP


#include <utility>
#include <string>

#include "../geometry/orthobrick.hpp"
#include "../geometry/multipolyhedron.hpp"
#include "../geometry/polygonal_cylinder.hpp"
#include "../algorithms/polyhedron_crosses_polyhedron.hpp"
#include "../algorithms/polyhedron_inside_polyhedron.hpp"


typedef std::pair<std::shared_ptr<Polyhedron>,std::shared_ptr<Polyhedron>> TP;  // ternary particle


MultiPolyhedron ternary_composite(Polyhedron &matrix,
                                  std::vector<std::pair<TP, int> > hysto)
{
    srand (time(NULL));

    MultiPolyhedron result;
    result.insert_particle(std::string("matrix"), matrix);

    float xhi = matrix.facets()[0].vertices()[0].x();
    float xlo = xhi;
    float yhi = matrix.facets()[0].vertices()[0].y();
    float ylo = yhi;
    float zhi = matrix.facets()[0].vertices()[0].z();
    float zlo = zhi;
    for (const Polygon &f : matrix.facets())
        for (const Point &v : f.vertices())
          {
            xlo = std::min(xlo, v.x());
            ylo = std::min(ylo, v.y());
            zlo = std::min(zlo, v.z());
            xhi = std::max(xhi, v.x());
            yhi = std::max(yhi, v.y());
            zhi = std::max(zhi, v.z());
          }
    float lx = xhi - xlo;
    float ly = yhi - ylo;
    float lz = zhi - zlo;

    for (std::pair<TP, int> p : hysto)
      {
        // TODO (current way is not good)
        const size_t fails_allowed = 1000 * p.second;

        Polyhedron filler = *(p.first.first);
        Polyhedron interface = *(p.first.second);

        size_t done = 0;
        size_t fails = 0;
        while (fails < fails_allowed && done < p.second)
          {
            float ax = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float ay = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float az = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float dx = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * lx;
            float dy = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * ly;
            float dz = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * lz;

            Polyhedron filler_try = filler;
            filler_try.translate(matrix.center().x() - filler_try.center().x(),
                                 matrix.center().y() - filler_try.center().y(),
                                 matrix.center().z() - filler_try.center().z());
            filler_try.rotate(filler_try.center(), ax,ay,az);
            filler_try.translate(dx,dy,dz);

            // If:  --- poly is completely inside matrix
            //      --- poly does not cross with all other poly's
            if (!polyhedron_inside_polyhedron(filler_try, matrix))
              {
                //std::cout << "Cross box\n";
                fails++;
                continue;
              }

            bool crosses_existing = false;
            for (Polyhedron &old : result.phases_content()["filler"])
                if (polyhedron_crosses_polyhedron(filler_try, old))
                  {
                    crosses_existing = true;
                    break;
                  }

            if (crosses_existing)
              {
                //std::cout << "Cross old\n";
                fails++;
                continue;
              }

            //std::cout << "Appended\n";

            Polyhedron interface_try = interface;
            interface_try.translate(matrix.center().x() - interface_try.center().x(),
                                    matrix.center().y() - interface_try.center().y(),
                                    matrix.center().z() - interface_try.center().z());
            interface_try.rotate(interface_try.center(), ax,ay,az);
            interface_try.translate(dx,dy,dz);

            result.insert_particle(std::string("filler"), filler_try);
            result.insert_particle(std::string("interface"), interface_try);
            done++;
          }
      }

    std::vector<std::string> phases_hierarchy = {"matrix", "interface", "filler"};
    result.set_phases_hierarchy(phases_hierarchy);

    return result;
};


/*
// For some reason, this does not work.
// Though struture looks ok, produced mesh gets broken.
std::ostream& output_ternary_system(std::ostream& os, MultiPolyhedron& ts)
{
    if (ts.phases_content().size() != 3)
      {
        throw std::string("Exception in "
                          "std::ostream& output_ternary_system(std::ostream&, MultiPolyhedron&): "
                          "Phases count in ternary system != 3");
      }

    auto matrix = ts.phases_content()["matrix"][0];
    float xhi = matrix.facets()[0].vertices()[0].x();
    float xlo = xhi;
    float yhi = matrix.facets()[0].vertices()[0].y();
    float ylo = yhi;
    float zhi = matrix.facets()[0].vertices()[0].z();
    float zlo = zhi;
    for (const Polygon &f : matrix.facets())
        for (const Point &v : f.vertices())
          {
            xlo = std::min(xlo, v.x());
            ylo = std::min(ylo, v.y());
            zlo = std::min(zlo, v.z());
            xhi = std::max(xhi, v.x());
            yhi = std::max(yhi, v.y());
            zhi = std::max(zhi, v.z());
          }
    os << "solid matrix_tmp = orthobrick(" << xlo << "," << ylo << "," << zlo
       << "; " << xhi << "," << yhi << "," << zhi << ");\n";

    std::vector<std::string> phases_hierarchy = ts.phases_hierarchy();

    for (auto kv : ts.phases_content())
      {
        if (kv.first == "matrix")
            continue;

        os << "solid " << kv.first << "_tmp = (";
        for (size_t idx = 0; idx < kv.second.size(); ++idx)
          {
            os << kv.second[idx];
            if (idx != kv.second.size() - 1)
                os << ") or (";
          }
        os << ");\n";
      }

    os << "\n";
    os << "solid filler = filler_tmp;\n";
    os << "solid interface = interface_tmp and not filler and matrix_tmp;\n";
    os << "solid matrix = matrix_tmp and not filler and not interface;\n";

    return os;
}
*/


std::ostream& output_ternary_system_every_particle(std::ostream& os, MultiPolyhedron& ts)
{
    if (ts.phases_content().size() != 3)
      {
        throw std::string("Exception in "
                          "std::ostream& output_ternary_system(std::ostream&, MultiPolyhedron&): "
                          "Phases count in ternary system != 3");
      }

    size_t N = ts.phases_content()["filler"].size();

    auto matrix = ts.phases_content()["matrix"][0];
    float xhi = matrix.facets()[0].vertices()[0].x();
    float xlo = xhi;
    float yhi = matrix.facets()[0].vertices()[0].y();
    float ylo = yhi;
    float zhi = matrix.facets()[0].vertices()[0].z();
    float zlo = zhi;
    for (const Polygon &f : matrix.facets())
        for (const Point &v : f.vertices())
          {
            xlo = std::min(xlo, v.x());
            ylo = std::min(ylo, v.y());
            zlo = std::min(zlo, v.z());
            xhi = std::max(xhi, v.x());
            yhi = std::max(yhi, v.y());
            zhi = std::max(zhi, v.z());
          }
    std::string matrix_string = "orthobrick(";
    matrix_string += std::to_string(xlo) + "," + std::to_string(ylo) + ","
                     + std::to_string(zlo) + "; " + std::to_string(xhi)
                     + "," + std::to_string(yhi) + "," + std::to_string(zhi) + ")";

    std::vector<std::string> phases_hierarchy = ts.phases_hierarchy();

    for (auto kv : ts.phases_content())
      {
        if (kv.first == "matrix")
            continue;

        for (size_t idx = 0; idx < kv.second.size(); ++idx)
          {
            os << "solid " << kv.first << "_" << idx << " = (";
            os << kv.second[idx] << ")";
            os << " and " << matrix_string;
            os << ";\n";
          }
      }

    os << "\n";
    os << "solid filler = ";
    for (size_t idx = 0; idx < N; ++idx)
      {
        os << "filler_" << idx;
        if (idx != N - 1)
            os << " or ";
        else
            os << ";\n";
      }
    os << "solid interface = not filler and (";
    for (size_t idx = 0; idx < N; ++idx)
      {
        os << "interface_" << idx;
        if (idx != N - 1)
            os << " or ";
        else
            os << ");\n";
      }

    os << "solid matrix = " << matrix_string << " and not filler and not interface;\n";

    return os;
}


#endif  // #ifndef SRC_CONSTRUCTORS_TERNARY_COMPOSITE_HPP
