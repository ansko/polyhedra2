#ifndef SRC_CONSTRUCTORS_BINARY_COMPOSITE_HPP
#define SRC_CONSTRUCTORS_BINARY_COMPOSITE_HPP


#include <utility>
#include <string>

#include "../geometry/orthobrick.hpp"
#include "../geometry/multipolyhedron.hpp"
#include "../geometry/polygonal_cylinder.hpp"
#include "../algorithms/polyhedron_crosses_polyhedron.hpp"
#include "../algorithms/polyhedron_inside_polyhedron.hpp"


// Matrix is defined as Polyhedron.
// Fillers are defined as Polyhedrons and thiers counts
// (each filler is transformed during structure formation).
MultiPolyhedron binary_composite(Polyhedron &matrix,
                                 std::vector<std::pair<std::shared_ptr<Polyhedron>, int> > hysto_fillers)
{
    srand (time(NULL));

    MultiPolyhedron result;
    result.insert_particle(std::string("matrix"), matrix);

    float xhi = matrix.facets()[0].vertices()[0].x();
    float xlo = xhi;
    float yhi = matrix.facets()[0].vertices()[0].y();
    float ylo = yhi;
    float zhi = matrix.facets()[0].vertices()[0].z();
    float zlo = zhi;
    for (const Polygon &f : matrix.facets())
        for (const Point &v : f.vertices())
          {
            xlo = std::min(xlo, v.x());
            ylo = std::min(ylo, v.y());
            zlo = std::min(zlo, v.z());
            xhi = std::max(xhi, v.x());
            yhi = std::max(yhi, v.y());
            zhi = std::max(zhi, v.z());
          }
    float lx = xhi - xlo;
    float ly = yhi - ylo;
    float lz = zhi - zlo;

    for (auto p : hysto_fillers)
      {
        // TODO (current way is not good)
        const size_t fails_allowed = 1000 * p.second;

        size_t done = 0;
        size_t fails = 0;
        while (fails < fails_allowed && done < p.second)
          {
            float ax = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float ay = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float az = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2*M_PI;
            float dx = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * lx;
            float dy = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * ly;
            float dz = (static_cast<float>(rand()) / static_cast<float>(RAND_MAX) - 0.5) * lz;

            Polyhedron poly = *p.first;
            poly.translate(matrix.center().x() - poly.center().x(),
                           matrix.center().y() - poly.center().y(),
                           matrix.center().z() - poly.center().z());
            poly.rotate(poly.center(), ax,ay,az);
            poly.translate(dx,dy,dz);

            // If:  --- poly is completely inside matrix
            //      --- poly does not cross with all other poly's
            if (!polyhedron_inside_polyhedron(poly, matrix))
              {
                //std::cout << "Cross box\n";
                fails++;
                continue;
              }

            bool crosses_existing = false;
            for (Polyhedron &old : result.phases_content()["filler"])
                if (polyhedron_crosses_polyhedron(poly, old))
                  {
                    crosses_existing = true;
                    break;
                  }

            if (crosses_existing)
              {
                //std::cout << "Cross old\n";
                fails++;
                continue;
              }

            //std::cout << "Appended\n";

            result.insert_particle(std::string("filler"), poly);
            done++;

          }
      }

    std::vector<std::string> phases_hierarchy = {"matrix", "filler"};
    result.set_phases_hierarchy(phases_hierarchy);

    return result;
};


#endif  // #ifndef SRC_CONSTRUCTORS_BINARY_COMPOSITE_HPP
